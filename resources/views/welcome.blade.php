@extends('layout.app')
@section('judul')
Halaman Index
@endsection
@section('content')
    <h1>SELAMAT DATANG! {{ $first_name }} {{$last_name}}</h1>
    <h3>Terima kasih telah bergabung di Website Kami. Media Belajar kita Bersama!</h3>
@endsection