@extends('layout.app')
@section('judul')
Halaman Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/cast/{{$value->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5" style="text-align: center;">No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection