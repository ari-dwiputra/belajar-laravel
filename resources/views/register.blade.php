@extends('layout.app')
@section('judul')
Halaman Form
@endsection
@section('content')
	<h2>Buat Account Baru</h2>
	<h3>Sign Up Form</h3>
	<form method="POST" action="{{ route('welcome') }}">
    @csrf
		<label>First Name :</label><br><br>
		<input required type="text" name="first_name"><br><br>

		<label>Last Name :</label><br><br>
		<input required type="text" name="last_name"><br><br>

		<label>Gender</label><br><br>
		<input id="male" type="radio" name="gender" value="male"><label for="male">Male</label><br>
		<input id="female" type="radio" name="gender" value="female"><label for="female">Female</label><br><br>

		<label>Nationality</label><br><br>
		<select name="nationality">
			<option value="Indonesia">Indonesia</option>
			<option value="Amerika">Amerika</option>
			<option value="Inggris">Inggris</option>
		</select><br><br>

		<label>Language Spoken</label><br><br>
		<input id="indonesia" type="checkbox" name="language" value="indonesia"><label for="indonesia">Bahasa Indonesia</label><br>
		<input id="english" type="checkbox" name="language" value="english"><label for="english">English</label><br>
		<input id="other" type="checkbox" name="language" value="other"><label for="other">Other</label><br><br>

		<label>Bio</label><br><br>
		<textarea name="bio" rows="10" cols="30"></textarea><br>

		<input type="submit" value="Sign Up">
	</form>
@endsection