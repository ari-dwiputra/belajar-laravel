<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home')->name('home');
Route::get('/data-tables', 'IndexController@dataTable')->name('data-tables');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/cast', 'CastController@index')->name('cast');
Route::get('/cast/create', 'CastController@create')->name('cast/create');
Route::post('/cast', 'CastController@store')->name('cast');
Route::get('/cast/{cast_id}', 'CastController@show')->name('cast');
Route::get('/cast/{cast_id}/edit', 'CastController@edit')->name('cast/edit');
Route::put('/cast/{cast_id}', 'CastController@update')->name('cast');
Route::delete('/cast/{cast_id}', 'CastController@destroy')->name('cast');


